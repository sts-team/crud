<?php

use Illuminate\Database\Seeder;

class PhoneBookGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
	DB::statement('TRUNCATE TABLE phonebook_group');

        factory(App\PhoneBookGroup::class, 10)->create();
    }
}
