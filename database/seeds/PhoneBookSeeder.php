<?php

use Illuminate\Database\Seeder;

class PhoneBookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
	DB::statement("TRUNCATE TABLE phonebook");

	factory(App\PhoneBook::class, 50)->create();
    }
}
