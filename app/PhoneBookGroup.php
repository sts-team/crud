<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhoneBookGroup extends Model
{
    //
    protected $table = 'phonebook_group';
}
